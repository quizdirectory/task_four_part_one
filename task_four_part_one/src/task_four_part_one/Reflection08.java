package task_four_part_one;
import java.lang.reflect.*;

public class Reflection08 {
	
  public static void main(String[] args) throws Exception {
	  
    Calculate calObj = new Calculate();
    
    Field[] fields = calObj.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", fields.length);
    
    for (Field field : fields) {
      field.setAccessible(true);
      double value = field.getDouble(calObj);
      value++;
      field.setDouble(calObj, value);
      System.out.printf("field name=%s type=%s value=%f\n", field.getName(),field.getType(), field.getDouble(calObj));
      
    }
    
  }
  
}
