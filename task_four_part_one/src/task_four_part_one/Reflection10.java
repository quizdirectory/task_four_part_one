package task_four_part_one;
import java.lang.reflect.*;

public class Reflection10 {
	
  public static void main(String[] args) throws Exception {
	  
    Calculate calObj = new Calculate();
    Method method = calObj.getClass().getDeclaredMethod("setValueOne", double.class);
    method.setAccessible(true);
    method.invoke(calObj, 10);
    System.out.println(calObj);
    
  }
  
}
