package task_four_part_one;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection09 {
	
  public static void main(String[] args) throws Exception {
	  
    Calculate calObj = new Calculate();
    
    Method[] methods = calObj.getClass().getMethods();
    System.out.printf("There are %d methods\n", methods.length);

    for (Method method : methods) {
    	
      System.out.printf("method name=%s type=%s parameters = ", method.getName(), method.getReturnType());
      
      Class[] types = method.getParameterTypes();
      
      for (Class cls : types) {
    	  
        System.out.print(cls.getName() + " ");
        
      }
      
      System.out.println();
      
    }
    
  }
  
}
