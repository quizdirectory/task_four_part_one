package task_four_part_one;
import java.lang.reflect.*;

public class Reflection11 {
	
	public static void main(String[] args) {
	   Calculate calObj = new Calculate();
	   Class calClass = calObj.getClass();
	
	   Method[] methods = calClass.getDeclaredMethods();
	
	   for (Method method : methods) {
	      System.out.printf("%s %s",method.getReturnType(), method.getName());
	      System.out.println();
	   }
	 }

}
