package task_four_part_one;

public class Calculate {

  public double valueOne = 5;
  private double valueTwo = 2;
  private double multiplier = 1;

  public Calculate() {
	  
  }

  public Calculate(double valueOne, double valueTwo) {
    this.valueOne = valueOne;
    this.valueTwo = valueTwo;
  }

  public void multipliedValueOne() {
    this.valueOne *= this.multiplier;
  }

  private void multipliedValueTwo() {
    this.valueTwo *= this.multiplier;
  }

  public double getValueOne() {
    return this.valueOne;
  }

  private void setValueOne(double valueOne) {
    this.valueOne = valueOne;
  }

  public double getValueTwo() {
    return valueTwo;
  }

  public void setValueTwo(double valueTwo) {
    this.valueTwo = valueTwo;
  }
  
  public double getMultiplier() {
	return multiplier;
  }
  
  public void setMultiplier(double multiplier) {
	this.multiplier = multiplier;
  }

  public String toString() {
    return String.format("(Value of valueOne is: %f, Value of valueTwo is: %f, Value of multiplier is: %f)", valueOne, valueTwo,multiplier);
  }
  
}
