package task_four_part_one;

public class Reflection03 {
	public static void main(String[] args) {
		Calculate calObj = new Calculate();
		System.out.println("class = " + calObj.getClass());
		System.out.println("class name = " + calObj.getClass().getName());
	}
}
