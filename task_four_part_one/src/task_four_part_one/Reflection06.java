package task_four_part_one;
import java.lang.reflect.Field;

public class Reflection06 {
  public static void main(String[] args) throws Exception {
	  
    Calculate calObj = new Calculate();
    Field[] fields = calObj.getClass().getDeclaredFields();
    
    System.out.printf("There are %d fields\n", fields.length);

    for (Field field : fields) {
      System.out.printf("field name=%s type=%s accessible=%s\n", field.getName(),field.getType(), field.canAccess(calObj));
    }
    
  }
  
}
